package prognoze;

import java.io.File;
import java.math.BigInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.util.ValidationEventCollector;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.xml.sax.SAXException;
import prognoze.Prognoze.Prognoza;
import prognoze.Prognoze.Prognoza.Datum;
import prognoze.Prognoze.Prognoza.Lokacija;
import prognoze.Prognoze.Prognoza.Lokacija.Drzava;
import prognoze.Prognoze.Prognoza.Temperatura;

/**
 *
 * @author ivanlesk
 */
public class MojePrognoze {

    private ObjectFactory objectFactory;
    private Prognoze prognoze;

    private String xmlFilePath = "C:\\Users\\ivanlesk\\Desktop\\algebra\\iss\\lab4\\xml.xml";

    public MojePrognoze() {
        this.objectFactory = new ObjectFactory();
        this.prognoze = objectFactory.createPrognoze();
    }

    public void make(Datum datum, XMLGregorianCalendar dohvacena, String drzava, String iso, String grad, String opis, String jedinica, int stupnjevi) {
        Prognoza prog = objectFactory.createPrognozePrognoza();

        Drzava country = new Drzava();
        country.setIso(iso);
        country.setValue(drzava);

        Lokacija loc = new Lokacija();
        loc.setGrad(grad);
        loc.setDrzava(country);

        Temperatura temperatura = new Temperatura();

        temperatura.setJedinica(jedinica);
        BigInteger bi = BigInteger.valueOf(stupnjevi);
        temperatura.setValue(bi);

        prog.setDatum(datum);
        prog.setDohvacena(dohvacena);
        prog.setLokacija(loc);
        prog.setOpis(opis);
        prog.setTemperatura(temperatura);

        prognoze.getPrognoza().add(prog);
    }

    public void marshal() {
        try {
            JAXBContext jc = JAXBContext.newInstance(Prognoze.class);
            Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            //m.setSchema(getSchema());
            m.marshal(prognoze, new File(xmlFilePath));
            m.marshal(prognoze, System.out);
        } catch (JAXBException ee) {
            System.out.println(ee.getMessage());
        }
    }

    public Schema getSchema() {
        Schema schema;
        try {
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            schema = sf.newSchema(new File("C:\\Users\\ivanlesk\\Desktop\\algebra\\iss\\lab4\\prognoze.xsd"));
        } catch (SAXException ex) {
            schema = null;
            System.out.println("Error loading file");
        }
        return schema;
    }

    public void unmarshal() {
        ValidationEventCollector vec = new JAXB2ValidationEventCollector();
        try {
            JAXBContext jc = JAXBContext.newInstance(Prognoze.class);

            Unmarshaller u = jc.createUnmarshaller();
            u.setSchema(getSchema());
            u.setEventHandler(vec);
            u.unmarshal(new File(xmlFilePath));

        } catch (JAXBException ex) {
            System.out.println("Validation error");
        } finally {
            if (vec.hasEvents()) {
                for (ValidationEvent ve : vec.getEvents()) {
                    System.out.println(ve.getMessage());
                }
            }
        }
    }
}
