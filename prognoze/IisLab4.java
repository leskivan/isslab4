/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prognoze;

import java.math.BigInteger;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import prognoze.Prognoze.Prognoza.Datum;
import prognoze.Prognoze.Prognoza.Lokacija;
import prognoze.Prognoze.Prognoza.Lokacija.Drzava;
import prognoze.Prognoze.Prognoza.Temperatura;

/**
 *
 * @author ivanlesk
 */
public class IisLab4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // TODO code application logic here

            Datum datum1 = new Datum();
            datum1.setDan("Ponedjxeljak");
            datum1.setVrijeme(DatatypeFactory.newInstance().newXMLGregorianCalendar("12:45:23"));

            Datum datum2 = new Datum();
            datum2.setDan("Srijeda");
            datum2.setVrijeme(DatatypeFactory.newInstance().newXMLGregorianCalendar("12:45:23"));

            Datum datum3 = new Datum();
            datum3.setDan("Petak");
            datum3.setVrijeme(DatatypeFactory.newInstance().newXMLGregorianCalendar("12:45:23"));

            XMLGregorianCalendar dohvacena1 = DatatypeFactory.newInstance().newXMLGregorianCalendar("2017-10-28T12:45:23");
            XMLGregorianCalendar dohvacena2 = DatatypeFactory.newInstance().newXMLGregorianCalendar("2017-10-29T12:45:23");
            XMLGregorianCalendar dohvacena3 = DatatypeFactory.newInstance().newXMLGregorianCalendar("2017-10-30T12:45:23");

            MojePrognoze mp = new MojePrognoze();

            mp.make(datum1, dohvacena1, "Hrvatska", "hr", "Zagreb", "Kišno vrijeme", "C", 10);
            mp.make(datum2, dohvacena2, "Njemačka", "des", "Stutgart", "Ok vrijeme", "C", 25);
            mp.make(datum3, dohvacena3, "Slovenija", "sl", "Maribor", "Oblačno vrijeme", "C", 10);

            mp.marshal();
            mp.unmarshal();
        } catch (DatatypeConfigurationException ex) {
            System.out.println(ex.getMessage());
        }

    }

}
